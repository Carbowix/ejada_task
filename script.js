const usersURL = 'https://jsonplaceholder.typicode.com/users';
const postsURL = 'https://jsonplaceholder.typicode.com/posts?userId=';
let currentActiveTab = null;
async function getAllUsers() {
  try {
    const response = await fetch(usersURL);
    if (!response.ok) {
      throw new Error('Failed to fetch users');
    }
    const users = await response.json();
    return users;
  } catch (error) {
    console.error(error);
    alert('Failed to fetch users: ' + error);
  }
}

async function getUserPosts(userId) {
  try {
    const response = await fetch(postsURL + userId);
    if (!response.ok) {
      throw new Error('Failed to fetch user posts');
    }
    const posts = await response.json();
    return posts;
  } catch (error) {
    console.error(error);
    alert('Failed to fetch user posts: ' + error);
  }
}

function displayPosts(posts) {
  const content = document.querySelector('div.tab-content');
  posts.forEach((post) => {
    const postCard = document.createElement('div');
    postCard.classList.add('post');
    postCard.innerHTML = `
        <h3>${post.title}</h3>
        <p>${post.body}</p>
      `;
    content.appendChild(postCard);
  });
}

function createTab(user) {
  //! <li><button class="active">Username</button></li>
  const tab = document.createElement('li');
  const button = document.createElement('button');
  button.textContent = user.username;
  button.addEventListener('click', async function () {
    if (currentActiveTab) currentActiveTab.classList.remove('active');
    this.classList.add('active');
    currentActiveTab = this;
    const content = document.querySelector('div.tab-content');
    content.innerHTML = ''; // Empty existing posts
    const loading = document.querySelector('div.loading');
    loading.classList.add('active');
    const posts = await getUserPosts(user.id);
    loading.classList.remove('active');
    displayPosts(posts);
  });
  tab.appendChild(button);
  return tab;
}

document.addEventListener('DOMContentLoaded', async function () {
  const users = await getAllUsers();
  const tabs = document.querySelector('ul.tabs');
  if (typeof users == 'object' && users.length > 0) {
    users.forEach((user) => {
      const tab = createTab(user);
      tabs.appendChild(tab);
    });
  }
  document.querySelectorAll('li > button')[0].click();
});
